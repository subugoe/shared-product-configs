# Shared Product Configs

Repository holds shared (public available) configs for different products.

# Updating

* Commit your changes
* Add a Git tag:
```
git tag -a v0.5.2 -m "Updated configuration"
git push origin --tags
```
* Update `go.mod` of the project that uses the module and set the version to the tag given above:
```
gitlab.gwdg.de/subugoe/shared-product-configs v0.5.2
```
* Run for your module:
```
go mod tidy
```

# Configuration

For testing purposes it's possible to override the bundled configurations files using an environment variable. This is for example  needed if you're testing against a bucket with another name then the provided config files.
Just provide a adopted configuration file from the [`sub/productcfg`](sub/productcfg) and rename it to the name of your bucket.

Add a volume mapping from your local changed config to a location inside your container:
```
    volumes:
      - ./cfg/local/productcfg/indexer.json:/etc/env/productcfg/indexer.json
```

Pass `PRODUCT_CFG_PATH` pointing to your configuration path inside the container:
```
PRODUCT_CFG_PATH=/etc/env/productcfg
```
