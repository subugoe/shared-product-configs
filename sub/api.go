package nlh

import (
	"bufio"
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

var (
	//go:embed productcfg/*.json
	//go:embed productcfg/*_anchor_ids.txt
	f embed.FS
)

// ProductConfig ...
type ProductConfig struct {
	Name           string
	Type           string `json:"type,omitempty"`
	InRegex        string `json:"in_regex,omitempty"`
	YearRegex      string `json:"year_regex,omitempty"`
	BaseDir        string `json:"base_dir,omitempty"`
	FulltextDir    string `json:"fulltext_dir,omitempty"`
	InSuffix       string `json:"in_suffix,omitempty"`
	InBucket       string `json:"in_bucket,omitempty"`
	InKey          string `json:"in_key,omitempty"`
	InPrefix       string `json:"in_prefix,omitempty"`
	OutBucket      string `json:"out_bucket,omitempty"`
	OutKey         string `json:"out_key,omitempty"`
	OutPrefix      string `json:"out_prefix,omitempty"`
	PdfOutKey      string `json:"pdf_out_key,omitempty"`
	Packaging      string `json:"packaging,omitempty"`
	Storage        string `json:"storage,omitempty"`
	ForceOverwrite string `json:"force_overwrite,omitempty"`
	FType          string `json:"ftype,omitempty"`
}

// GetProductCfg will retrieve a product name and doctype and return the 'ProductConfig,nil'.
// If GetProductCfg encounters any errors, it will return 'nil,error'.
func GetProductCfg(product string, doctype string) (*ProductConfig, error) {
	var data []byte
	var err error
	var productCfgPath string
	productCfgPathPrefix := os.Getenv("PRODUCT_CFG_PATH")

	if productCfgPathPrefix == "" {
		productCfgPath := "productcfg/" + product + ".json"
		data, err = f.ReadFile(productCfgPath)
		if err != nil {
			return nil, fmt.Errorf("Could not find config for product '%s' internally, due to '%s', - try to set PRODUCT_CFG_PATH to set lookup path for '%s.json'", product, err, product)
		}
	} else {
		productCfgPath := productCfgPathPrefix + "/" + product + ".json"
		data, err = ioutil.ReadFile(productCfgPath)
		if err != nil {
			return nil, fmt.Errorf("Could not find config for product '%s' in '%s', due to '%s', trying local files", product, productCfgPathPrefix, err)
		}
	}

	productCfg := viper.New()
	//productCfg.AddConfigPath(productCfgPath)
	productCfg.SetConfigType("json")
	//productCfg.SetConfigName(product)
	//if err := productCfg.ReadInConfig(); err != nil {
	if err := productCfg.ReadConfig(bytes.NewBuffer(data)); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return nil, fmt.Errorf("config file for product %s, due to %s", product, err)
		}
		return nil, fmt.Errorf("config file for product %s found but an error was produced, due to %s", product, err.Error())
	}

	cfgMetsProperties := productCfg.GetStringMapString(doctype)
	jsonbody, err := json.Marshal(cfgMetsProperties)
	if err != nil {
		return nil, fmt.Errorf("could not marshal %s in %s, due to %s", doctype, productCfgPath, err)
	}

	productConfig := &ProductConfig{}
	if err := json.Unmarshal(jsonbody, &productConfig); err != nil {
		return nil, fmt.Errorf("could not unmarshal %s to ProductConfig{}, due to %s", jsonbody, err)
	}

	return productConfig, nil
}

// GetAnchorIDs will retrieve a list of ID for their anchors and returns 'map[string]string,nil', with documentId as key and 'anchor' as value.
// If GetAnchorIDs encounters any errors, it will return 'nil,error'.
func GetAnchorIDs(product string) (map[string]string, error) {

	var idToDoctype map[string]string = make(map[string]string)

	var err error
	var anchorListPath string = "productcfg/" + product + "_anchor_ids.txt"

	anchorIDsFile, err := f.Open(anchorListPath)
	if err != nil {
		return idToDoctype, fmt.Errorf("%s", err)
	}

	scanner := bufio.NewScanner(anchorIDsFile)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		idToDoctype[scanner.Text()] = "anchor"
	}
	anchorIDsFile.Close()
	return idToDoctype, nil

}
