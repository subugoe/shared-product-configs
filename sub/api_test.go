package nlh

import (
	"reflect"
	"testing"
)

func TestGetProductCfg(t *testing.T) {
	type args struct {
		product string
		doctype string
	}
	tests := []struct {
		name    string
		args    args
		want    *ProductConfig
		wantErr bool
	}{
		{
			"no error",
			args{
				product: "nlh-mme",
				doctype: "mets",
			},
			&ProductConfig{
				Type: "work_level",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetProductCfg(tt.args.product, tt.args.doctype)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetProductCfg() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Type, tt.want.Type) {
				t.Errorf("GetProductCfg() = %v, want %v", got, tt.want)
			}
		})
	}
}
